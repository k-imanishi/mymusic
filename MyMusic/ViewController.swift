//
//  ViewController.swift
//  MyMusic
//
//  Created by 今西 健二 on 2017/08/06.
//  Copyright © 2017年 今西 健二. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // シンバルの音源ファイルを指定
//    let cymbalPath = Bundle.main.bundleURL.appendPathComponent("cymbal.mp3")
    let cymbalPath = Bundle.main.url(forResource: "cymbal", withExtension: "mp3");
    // シンバル用のプレイヤーインスタンスを作成
    var cymbalPlayer = AVAudioPlayer();
    // ギターの音源ファイルを指定
    let guitarPath = Bundle.main.url(forResource: "guitar", withExtension: "mp3");
    // ギター用のプレイヤーインスタンスを作成
    var guitarPlayer = AVAudioPlayer();
    // BGMの音源ファイルを指定
    let backmusicPath = Bundle.main.url(forResource: "backmusic", withExtension: "mp3");
    // BGM用のプレイヤーインスタンスを作成
    var backmusicPlayer = AVAudioPlayer();

    @IBAction func cymbal(_ sender: Any) {
        soundPlayer(&cymbalPlayer, path:cymbalPath!, count:0);
    }

    @IBAction func guitar(_ sender: Any) {
        soundPlayer(&guitarPlayer, path:guitarPath!, count:0);
    }
    
    @IBAction func play(_ sender: Any) {
        soundPlayer(&backmusicPlayer, path:backmusicPath!, count:0);
    }
    
    @IBAction func stop(_ sender: Any) {
        backmusicPlayer.stop();
    }
    
    fileprivate func soundPlayer(_ player:inout AVAudioPlayer, path: URL, count: Int) {
        do {
            player = try AVAudioPlayer(contentsOf: path, fileTypeHint: nil);
            player.numberOfLoops = count;
            player.play();
        } catch {
            print("エラー発生");
        }
    }
    
}

